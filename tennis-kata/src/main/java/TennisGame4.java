import java.util.Objects;

public class TennisGame4 implements TennisGame {

  private final Player player1;

  private final Player player2;

  private TennisGame4(Player player1, Player player2) {
    Objects.requireNonNull(player1);
    Objects.requireNonNull(player2);
    this.player1 = player1;
    this.player2 = player2;
  }

  public static TennisGame4 fromPlayerNames(String player1, String player2) {
    return new TennisGame4(Player.create(player1), Player.create(player2));
  }

  @Override
  public void wonPoint(String playerName) {
    if (player1.getName().equals(playerName)) {
      player1.addPoint();
    } else if (player2.getName().equals(playerName)) {
      player2.addPoint();
    } else {
      throw new IllegalArgumentException("No player with name " + playerName + " is competing");
    }
  }

  @Override
  public String getScore() {
    if (inEarlyGame()) {
      String player1Score = ScoreName.fromScore(player1.getScore()).capitalised();
      String player2Score = ScoreName.fromScore(player2.getScore()).capitalised();
      return tying() ? player1Score + "-All"
          : player1Score + "-" + player2Score;
    }
    if (tying()) {
      return "Deuce";
    }
    return player1.scoreDifference(player2) == 1
        ? "Advantage " + nameOfPlayerAhead()
        : "Win for " + nameOfPlayerAhead();

  }

  private boolean inEarlyGame() {
    return player1.hasLessThanFourPoints()
        && player2.hasLessThanFourPoints()
        && !(bothPlayersAtThreePoints());
  }

  private boolean bothPlayersAtThreePoints() {
    return player1.getScore() == 3
        && player2.getScore() == 3;
  }

  private boolean tying() {
    return player2.sameScoreAs(player1);
  }

  private String nameOfPlayerAhead() {
    return player1.isAheadOf(player2)
        ? player1.getName()
        : player2.getName();
  }

  @Override
  public void resetGame() {
    player1.resetScore();
    player2.resetScore();
  }
}

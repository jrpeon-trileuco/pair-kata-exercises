import java.util.Map;
import java.util.Objects;

import com.google.api.client.util.Maps;

public class TennisGame1 implements TennisGame {

    private final String player1Name;
    private int player1Score = 0;
    private final String player2Name;
    private int player2Score = 0;

    public TennisGame1(String player1Name, String player2Name) {
        Objects.requireNonNull(player1Name);
        Objects.requireNonNull(player2Name);
        this.player1Name = player1Name;
        this.player2Name = player2Name;
    }

    public void wonPoint(String playerName) {
        Objects.requireNonNull(playerName);
        if (playerName.equals("player1")) {
            player1Score++;
        } else {
            player2Score++;
        }
    }

    public String getScore() {
        String score = "";
        if (isTie()) {
            score = getScoreForTie();
        } else if (isAdvantage()) {
            score = getScoreForAdvantage();
        } else {
            score = getEarlyGameScoreValue(score);
        }
        return score;
    }

    public int getPlayer1Score() {
        return player1Score;
    }

    public int getPlayer2Score() {
        return player2Score;
    }

    private boolean isAdvantage() {
        return player1Score >= 4 || player2Score >= 4;
    }

    private boolean isTie() {
        return player1Score == player2Score;
    }

    private String getScoreForTie() {
        Map<Integer, String> scoreStringByScoreValue = Maps.newHashMap();
        scoreStringByScoreValue.put(0, "Love-All");
        scoreStringByScoreValue.put(1, "Fifteen-All");
        scoreStringByScoreValue.put(2, "Thirty-All");
        return getPlayer1Score() < 3 ? scoreStringByScoreValue.get(getPlayer1Score()) : "Deuce";
    }

    private String getScoreForAdvantage() {
        int minusResult = getPlayer1Score() - getPlayer2Score();
        Map<Integer, String> scoreStringByScoreValue = Maps.newHashMap();
        scoreStringByScoreValue.put(1, "Advantage player1");
        scoreStringByScoreValue.put(-1, "Advantage player2");
        scoreStringByScoreValue.put(2, "Win for player1");
        scoreStringByScoreValue.put(3, "Win for player1");
        scoreStringByScoreValue.put(4, "Win for player1");
        return scoreStringByScoreValue.getOrDefault(minusResult, "Win for player2");
    }

    private String getEarlyGameScoreValue(String score) {
        Map<Integer, String> scoreStringByScoreValue = Maps.newHashMap();
        scoreStringByScoreValue.put(0, "Love");
        scoreStringByScoreValue.put(1, "Fifteen");
        scoreStringByScoreValue.put(2, "Thirty");
        scoreStringByScoreValue.put(3, "Forty");
        score += scoreStringByScoreValue.get(getPlayer1Score()) + "-" + scoreStringByScoreValue.get(getPlayer2Score());
        return score;
    }

    @Override
    public void resetGame() {
        this.player1Score = 0;
        this.player2Score = 0;
    }
}

import java.util.Objects;

public class TennisGame3 implements TennisGame {

    private static final String[] ORDERED_SCORE_NAMES = new String[]{"Love", "Fifteen", "Thirty", "Forty"};
    
    private int player1Score;
    private int player2Score;
    private String player1Name;
    private String player2Name;

    public TennisGame3(String player1Name, String player2Name) {
        Objects.requireNonNull(player1Name);
        Objects.requireNonNull(player2Name);
        this.player1Name = player1Name;
        this.player2Name = player2Name;
    }

    public String getScore() {
        if (inEarlyGame()) {
            String player1ScoreName = scoreName(player1Score);
            String player2ScoreName = scoreName(player2Score);
            return tying() ? player1ScoreName + "-All"
                : player1ScoreName + "-" + player2ScoreName;
        }
        if (tying()) {
            return "Deuce";
        }
        return scoreDifference() == 1 ? "Advantage " + nameOfPlayerAhead()
            : "Win for " + nameOfPlayerAhead();
    }

    private String nameOfPlayerAhead() {
        return player2Score > player1Score ? player2Name : player1Name;
    }

    private boolean tying() {
        return player2Score == player1Score;
    }

    private boolean inEarlyGame() {
        return player2Score < 4 && player1Score < 4 && !(player2Score + player1Score == 6);
    }

    private int scoreDifference() {
        return Math.abs(player2Score - player1Score);
    }

    public void wonPoint(String playerName) {
        Objects.requireNonNull(playerName);
        if (playerName.equals("player1")) {
            player1Score++;
        } else {
            player2Score++;
        }
    }

    @Override
    public void resetGame() {
        this.player1Score = 0;
        this.player2Score = 0;
    }

    private String scoreName(int score) {
        return ORDERED_SCORE_NAMES[score];
    }
}

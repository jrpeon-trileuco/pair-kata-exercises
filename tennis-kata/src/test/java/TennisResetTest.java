import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TennisResetTest {


  @Test
  public void tennisGame1_resetGame_shouldSetScoresToZero() {
    TennisGame1 game = new TennisGame1("player1", "player2");
    checkReset(game);
  }

  @Test
  public void tennisGame2_resetGame_shouldSetScoresToZero() {
    TennisGame2 game = new TennisGame2("player1", "player2");
    checkReset(game);
  }

  @Test
  public void tennisGame3_resetGame_shouldSetScoresToZero() {
    TennisGame3 game = new TennisGame3("player1", "player2");
    checkReset(game);
  }

  @Test
  public void tennisGame4_resetGame_shouldSetScoresToZero() {
    TennisGame4 game = TennisGame4.fromPlayerNames("player1", "player2");
    checkReset(game);
  }

  private void checkReset(TennisGame game) {
    assertEquals("Love-All", game.getScore());
    game.wonPoint("player1");
    assertEquals("Fifteen-Love", game.getScore());
    game.wonPoint("player2");
    assertEquals("Fifteen-All", game.getScore());
    game.resetGame();
    assertEquals("Love-All", game.getScore());
  }

}

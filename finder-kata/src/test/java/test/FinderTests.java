package test;
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import algorithm.AgeGap;
import algorithm.SearchMode;
import algorithm.AgeGapFinder;
import algorithm.Person;

public class FinderTests {

	Person sue = new Person();
	Person greg = new Person();
	Person sarah = new Person();
	Person mike = new Person();

	@Before
	public void setup() {
		sue.name = "Sue";
		sue.birthDate = new Date(50, 0, 1);
		greg.name = "Greg";
		greg.birthDate = new Date(52, 5, 1);
		sarah.name = "Sarah";
		sarah.birthDate = new Date(82, 0, 1);
		mike.name = "Mike";
		mike.birthDate = new Date(79, 0, 1);
	}

	@Test
	public void Returns_Empty_Results_When_Given_Empty_List() {
		List<Person> list = new ArrayList<Person>();
		AgeGapFinder finder = new AgeGapFinder(list);

		AgeGap result = finder.find(SearchMode.CLOSEST);
		assertEquals(null, result.younger);

		assertEquals(null, result.older);
	}

	@Test
	public void Returns_Empty_Results_When_Given_One_Person() {
		List<Person> list = new ArrayList<Person>();
		list.add(sue);

		AgeGapFinder finder = new AgeGapFinder(list);

		AgeGap result = finder.find(SearchMode.CLOSEST);

		assertEquals(null, result.younger);
		assertEquals(null, result.older);
	}

	@Test
	public void Returns_Closest_Two_For_Two_People() {
		List<Person> list = new ArrayList<Person>();
		list.add(sue);
		list.add(greg);
		AgeGapFinder finder = new AgeGapFinder(list);

		AgeGap result = finder.find(SearchMode.CLOSEST);

		assertEquals(sue, result.younger);
		assertEquals(greg, result.older);
	}

	@Test
	public void Returns_Furthest_Two_For_Two_People() {
		List<Person> list = new ArrayList<Person>();
		list.add(mike);
		list.add(greg);

		AgeGapFinder finder = new AgeGapFinder(list);

		AgeGap result = finder.find(SearchMode.FURTHEST);

		assertEquals(greg, result.younger);
		assertEquals(mike, result.older);
	}

	@Test
	public void Returns_Furthest_Two_For_Four_People() {
		List<Person> list = new ArrayList<Person>();
		list.add(sue);
		list.add(sarah);
		list.add(mike);
		list.add(greg);
		AgeGapFinder finder = new AgeGapFinder(list);

		AgeGap result = finder.find(SearchMode.FURTHEST);

		assertEquals(sue, result.younger);
		assertEquals(sarah, result.older);
	}

	@Test
	public void Returns_Closest_Two_For_Four_People() {
		List<Person> list = new ArrayList<Person>();
		list.add(sue);
		list.add(sarah);
		list.add(mike);
		list.add(greg);

		AgeGapFinder finder = new AgeGapFinder(list);

		AgeGap result = finder.find(SearchMode.CLOSEST);

		assertEquals(sue, result.younger);
		assertEquals(greg, result.older);
	}

}

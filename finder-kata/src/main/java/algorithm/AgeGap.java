package algorithm;

public class AgeGap implements Comparable<AgeGap> {

  public Person younger;

  public Person older;

  public long difference;

  private AgeGap(Person younger, Person older, long difference) {
    this.younger = younger;
    this.older = older;
    this.difference = difference;
  }

  public static AgeGap empty() {
    return new AgeGap(null, null, 0);
  }

  public static AgeGap fromComparison(Person person1, Person person2) {
    Person younger, older;
    if (person1.isYoungerThan(person2)) {
      younger = person1;
      older = person2;
    } else {
      younger = person2;
      older = person1;
    }
    return AgeGap.builder()
        .younger(younger)
        .older(older)
        .difference(older.birthDateDifference(younger))
        .build();
  }

  public static Builder builder() {
    return new Builder();
  }

  @Override
  public int compareTo(AgeGap other) {
    return Long.valueOf(this.difference)
        .compareTo(other.difference);
  }

  public static final class Builder {

    public Person younger;

    public Person older;

    public long difference;

    private Builder() {
    }

    public Builder younger(Person younger) {
      this.younger = younger;
      return this;
    }

    public Builder older(Person older) {
      this.older = older;
      return this;
    }

    public Builder difference(long difference) {
      this.difference = difference;
      return this;
    }

    public AgeGap build() {
      return new AgeGap(younger, older, difference);
    }
  }
}

package algorithm;

public enum SearchMode {
  CLOSEST,
  FURTHEST
}
